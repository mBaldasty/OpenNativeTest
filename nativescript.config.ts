import { NativeScriptConfig } from '@nativescript/core';

export default {
  id: 'org.nativescript.OpenNativeSample',
  appPath: 'app',
  appResourcesPath: 'App_Resources',
  android: {
    v8Flags: '--expose_gc',
    markingMode: 'none'
  },
  hooks: [
    {
      script: './customHooks/after-prepare.js',
      type: 'after-prepare'
    },
    {
      script: './customHooks/before-resolveCommand.js',
      type: 'after-resolveCommand'
    }
  ]
} as NativeScriptConfig;
