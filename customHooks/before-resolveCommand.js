const packageJsonPath = '../package.json';
const packageJsonTargetPath = './package.json'
const packageJson = require(packageJsonPath);
const child_process = require("child_process");
const fs = require('fs')

module.exports = function () {
  console.log("Running tasks before prepare");
  let reactNativeNFCManager = 'react-native-nfc-manager';
  let reactNativeNFCManagerVersion = '3.14.11'
  if (!packageJson.dependencies || !packageJson.dependencies[reactNativeNFCManager]) {
    console.log(`Add ${reactNativeNFCManager}`);

    packageJson.dependencies = packageJson.dependencies || {};
    packageJson.dependencies[reactNativeNFCManager] = reactNativeNFCManagerVersion;
    // child_process.execSync('yarn add '+reactNativeNFCManager+'@'+reactNativeNFCManagerVersion)
    const content = packageJson
    try {
      fs.writeFileSync(packageJsonTargetPath, JSON.stringify(content), { flag: 'w+' })
    } catch (e) {
      console.log(e)
    }
  } else {
    console.log(`Dependency ${reactNativeNFCManager} already present in package.json`);
  }
  console.log("Tasks before prepare completed.");
};
